# Tokyo Programming Meetups

The goal of this project is to interface with the Meetup API to get a concise
list of meetups in the Tokyo area.

That's it. Nothing more to it. I guess I'll serve up the list in a web front end
of some kind, but nothing fancy planned. I want a list of meetup titles, and I want
them all to pertain to technology in some way. No language exchange meetups, no 
meetups with just one person going - signal > noise. Because fuck spending 20
minutes in their web interface just to determine if there's anything worth attending
in the techsphere.